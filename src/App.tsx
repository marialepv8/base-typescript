import React from 'react';
import './App.css';
import {
  BrowserRouter as Router
} from "react-router-dom";
//import Layout from "./components/layout/Layout";
import Layout from "./components/layoutMui/Layout";

const App = () => {
  return (
    <Router>
      <Layout />
    </Router>
  );
}

export default App;
