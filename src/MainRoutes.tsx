import React from 'react';
import { Routes, Route } from 'react-router-dom';
import About from './components/about/About';
import Home from './components/home/Home';


const MainRoutes = () => {
  return (
    <Routes>
      <Route path='/' caseSensitive={false} element={<Home />} />
      <Route path='/about' caseSensitive={false} element={<About />} />
    </Routes>
  )
}

export default MainRoutes;