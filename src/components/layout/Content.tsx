import React from 'react';
import { Layout } from 'antd';

interface propsOptions {
  children: React.ReactNode;
}

const Content = ({children} : propsOptions) => {
  return (
    <Layout.Content style={{height: '100vh'}}>
      {children}
    </Layout.Content>
  )
}

export default Content;