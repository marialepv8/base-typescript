import { Layout as LayoutAnt } from 'antd';
//import Header from './Header';
import Sidebar from './Sidebar';
import Content from './Content';
//import Footer from './Footer';
import MainRoutes from '../../MainRoutes';

const Layout = () => {
  return (
    <LayoutAnt>
      <LayoutAnt>
        <Sidebar />
        <Content >
          <MainRoutes />
        </Content>
      </LayoutAnt>
    </LayoutAnt>
  )
}

export default Layout