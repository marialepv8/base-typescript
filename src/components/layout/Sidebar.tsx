import React, { useState } from 'react';
import { Button, Layout, Menu } from 'antd';
import { Link } from 'react-router-dom';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined
} from '@ant-design/icons';


const Sidebar = () => {
  const [ showMenu, setShowMenu ] = useState(true);
  return (
    <Layout.Sider 
      trigger={null} 
      collapsible 
      collapsed={showMenu} 
      style={{backgroundColor: '#fff'}}
    >
      <Button 
        type="primary" 
        onClick={() => setShowMenu(!showMenu)} 
        style={{ marginBottom: 16 }}
        icon={showMenu ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
      />
      <Menu 
        defaultSelectedKeys={['1']}
        mode="inline"
        theme="light"
        inlineCollapsed={showMenu}
      >
        <Menu.Item key={1} /* icon={<MenuUnfoldOutlined />} */>
          <Link to="/">1</Link>
        </Menu.Item>
        <Menu.Item key={2} /* icon={<MenuFoldOutlined />} */>
          <Link to="/about">2</Link>
        </Menu.Item>
      </Menu>
    </Layout.Sider>
  )
}

export default Sidebar