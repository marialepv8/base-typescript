import React from 'react';

interface propsOptions {
  children: React.ReactNode;
}

const Content = ({children} : propsOptions) => {
  return (
    <div style={{height: '100vh'}}>
      {children}
    </div>
  )
}

export default Content;