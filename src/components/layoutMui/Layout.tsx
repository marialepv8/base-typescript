import Sidebar from './Sidebar';
import Content from './Content';
import { Grid } from '@mui/material'
import MainRoutes from '../../MainRoutes';

const Layout = () => {
  return (
    <Grid container spacing={0.5}>
      <Grid item xs>
        <Sidebar />
      </Grid>
      <Grid item xs={10}>
        <Content >
          <MainRoutes />
        </Content>
      </Grid>
    </Grid>
  )
}

export default Layout