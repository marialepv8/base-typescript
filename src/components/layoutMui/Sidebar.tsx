import { Link } from 'react-router-dom';
import { MenuList, MenuItem, List, ListItemText } from '@mui/material';

const Sidebar = () => {
  return (
    <>
      <MenuList >
        <Link to="/">
          <MenuItem >
            Home
          </MenuItem>
        </Link>
        <Link to="/about">
          <MenuItem >
              Abaout
          </MenuItem>
        </Link>
      </MenuList>
    </>
  )
}

export default Sidebar;